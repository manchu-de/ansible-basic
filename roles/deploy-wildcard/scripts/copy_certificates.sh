#!/usr/bin/env bash

read -p "enter certificate domain (certable.net):" cert_path

echo copy certificates
cp /etc/letsencrypt/live/$cert_path/* /certable/fs23_jp_2/roles/deploy-wildcard/files/ssl

chmod 740 /certable/fs23_jp_2/roles/deploy-wildcard/files/ssl/*
