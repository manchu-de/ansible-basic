#!/bin/sh
# Create a self signed default certificate, so Ngix can start before we have
# any real certificates.


if [[ ! -f /certable/ssl/letsencrypt/certificates/fullchain.pem ]];then
    mkdir -p /certable/ssl/letsencrypt/certificates
fi

### If certificates don't exist yet we must ensure we create them to start nginx
if [[ ! -f /certable/ssl/letsencrypt/certificates/fullchain.pem ]]; then
  #  openssl genrsa -out /certable/ssl/letsencrypt/certificates/privkey.pem 4096
  #  openssl req -nodes -new -key /certable/ssl/letsencrypt/certificates/privkey.pem -out /certable/ssl/letsencrypt/certificates/cert.csr -subj \
  #      "/C=DE/ST=DUS/L=NRW/O=${DOMAIN:-certable.net}/OU=certable CN=${DOMAIN:-certable.net}/EMAIL=${EMAIL:-it-service@certable.com}"
    openssl req -nodes -newkey rsa:4096 -sha256 -keyout /certable/ssl/letsencrypt/certificates/privkey.pem -out /certable/ssl/letsencrypt/certificates/cert.csr -subj \
    "/C=DE/ST=DUS/L=NRW/O=${DOMAIN:-certable.net}/OU=certable CN=${DOMAIN:-certable.net}/EMAIL=${EMAIL:-it-service@certable.com}"
    openssl x509 -req -days 365 -in /certable/ssl/letsencrypt/certificates/cert.csr -signkey /certable/ssl/letsencrypt/certificates/privkey.pem -out /certable/ssl/letsencrypt/certificates/fullchain.pem
fi

### Send certbot Renewal to background
$(while :; do ./certbot.sh; sleep "${RENEW_INTERVAL:-12h}"; done;) &

### Start nginx with daemon off as our main pid
nginx -g "daemon off;"
